<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

class ContractForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('secondName', TextType::class, ['required' => false, 'attr' => ['class' => 'form-control']])
            ->add('lastName', TextType::class, ['required' => false, 'attr' => ['class' => 'form-control']])
            ->add('email', EmailType::class, ['attr' => ['class' => 'form-control']])
            ->add('phone', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('save', SubmitType::class,
                [
                    'label' => 'Save',
                    'attr' => ['class' => 'btn btn-success']
                ]);
    }
}