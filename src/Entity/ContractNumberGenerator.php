<?php

namespace App\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class ContractNumberGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity)
    {
        $entity_name = $em->getClassMetadata(get_class($entity))->getName();
        return \sprintf(
            'CC-%s-SL',
            \str_pad($em->getRepository($entity_name)->count([]) + 1,6, '0', STR_PAD_LEFT)
        );
    }

    public function isPostInsertGenerator()
    {
        return false;
    }
}