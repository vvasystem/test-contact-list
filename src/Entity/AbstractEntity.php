<?php

namespace App\Entity;

abstract class AbstractEntity
{
    final public function populate(array $data)
    {
        foreach ($data as $field => $value) {
            $setter = 'set' . \ucfirst($field);
            if (\method_exists($this, $setter)) {
                $this->{$setter}($value);
            } else {
                $this->{$field} = $value;
            }
        }
    }
}