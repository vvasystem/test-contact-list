<?php
namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContractForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="contact-list")
     */
    public function index()
    {
        $list = $this->getDoctrine()
                     ->getRepository(Contact::class)
                     ->findAll();
        return $this->render('default/index.html.twig', [
            'list' => $list,
        ]);
    }

    /**
     * @Route("/add", name="add-contact")
     */
    public function add(Request $request)
    {
        $contact = new Contact();

        $form = $this->createForm(ContractForm::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('contact-list');
        }

        return $this->render('default/add.html.twig', [
            'contact' => $contact,
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{number}", name="edit-contact")
     */
    public function edit($number, Request $request)
    {
        $contact = $this->getDoctrine()
                        ->getRepository(Contact::class)
                        ->find($number);
        if (!$contact) {
            throw $this->createNotFoundException('Contact not found');
        }

        $form = $this->createForm(ContractForm::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            return $this->redirectToRoute('contact-list');
        }

        return $this->render('default/edit.html.twig', [
            'contact' => $contact,
            'form'    => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{number}", name="delete-contact")
     */
    public function delete($number, Request $request)
    {
        $contact = $this->getDoctrine()
                        ->getRepository(Contact::class)
                        ->find($number);
        if (!$contact) {
            throw $this->createNotFoundException('Contact not found');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($contact);
        $entityManager->flush();

        return $this->redirectToRoute('contact-list');
    }

}