# ТЗ

Необходимо реализовать веб-страничку – список контактов с возможностью создания, изменения, удаления.

- Контакты должны храниться в базе данных (любой)
- При создании нового контакта ему должен быть присвоен уникальный номер. в формате «CC-{N}-SL», где N – порядковый номер контакта, N=1, 2, 3,… и т.д. CC-000001-SL, CC-000002-SL, CC-000003-SL, …, 
- Не должно появиться в базе данных контактов без фамилии, телефон, email
- При сохранении контакта должна работать проверка формата Телефона  x(xxx)xxxxxxx и Email.

## Requirements:
- Installed docker-compose 
- Current user in docker group (or sudo), `usermod -a -G docker $USER`
- Installed composer (or use docker image)
- Free `8000` port (can be changed in docker-compose.yml)   

## Install and Run
- `git clone https://git@bitbucket.org/vvasystem/test-contact-list.git`
- `cd test-contact-list/`
- `./bin/install.sh`
- `docker-compose -f docker-compose-base up -d`
- `./bin/migrate-up.sh` - if you have got the error `SQLSTATE[08006] [7] could not connect to server: Connection refused`, please try again later (it's normal because postgres not yet started)
- [http://127.0.0.1:8000]

## What next?

- added mutex for atomic generation contact number (use https://symfony.com/doc/current/components/lock.html)
- added unit tests
- added CI/DI
