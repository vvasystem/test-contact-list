#!/bin/bash

./bin/composer.sh install
./bin/npm.sh install
./bin/npm.sh rebuild node-sass sass-loader
./node_modules/.bin/encore dev
