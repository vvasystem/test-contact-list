#!/bin/bash
mkdir -p $HOME/.npm
docker run --rm \
    -u $UID:$UID \
    -v $(pwd):/app \
    -w /app \
    -v $HOME/.npm:/home/${USER}/.npm \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    ${ADDITIONAL_NPM_DOCKER_CONTAINER_OPTIONS} \
    node:6 npm $@